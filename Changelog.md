Changelog
=========

2.0.1 (XX/XX/XXXX)
------------------

Nothing planned for now.

2.0.0 (23/09/2014)
------------------

* Follow changes of laravel 4.2, notice that this is an important BCBreak and that's why the new version is 2.0.x
* Fixed tests
* Added continous integration to improve the quality of the code we provide
* Some adaptations for KnpMenu 2.0
* Fixed [issue #1](https://bitbucket.org/hailwoodnz/knp-menu-laravel4/issue/1/cant-install-on-laravel-41)
* Little review on documentation
* Follow PSR conventions
