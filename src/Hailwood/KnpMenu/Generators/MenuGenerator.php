<?php

namespace Hailwood\KnpMenu\Generators;

use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;

class MenuGenerator
{
    protected $files;

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    public function make($menu, $path, array $options = array())
    {
        if (!preg_match('#^[\w]+Menu$#', $menu)) {
            $menu = Str::camel($menu).'Menu';
        }

        $stub = $this->getMenu($menu = ucfirst($menu));

        if (!$this->files->exists($fullPath = $path."/{$menu}.php")) {
            return $this->files->put($fullPath, $stub);
        }

        return false;
    }

    public function getMenu($menu)
    {
        $stub = $this->files->get(__DIR__.'/../Resources/stubs/menu.php');

        return str_replace('{{class}}', $menu, $stub);
    }
}
