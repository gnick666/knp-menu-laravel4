<?php

namespace Hailwood\KnpMenu\Renderer;

use Knp\Menu\Renderer\RendererProviderInterface;
use Illuminate\Container\Container;

class ContainerAwareProvider implements RendererProviderInterface
{
    private $container;
    private $default;
    private $renderers;

    public function __construct(Container $container, $default, array $renderers)
    {
        $this->container = $container;
        $this->default   = $default;
        $this->renderers = $renderers;
    }

    public function get($name = null)
    {
        if (null === $name) {
            $name = $this->default;
        }

        if (!isset($this->renderers[$name])) {
            throw new \InvalidArgumentException(sprintf('The renderer "%s" is not defined.', $name));
        }

        return $this->container[$this->renderers[$name]];
    }

    public function has($name)
    {
        return isset($this->renderers[$name]);
    }
}
