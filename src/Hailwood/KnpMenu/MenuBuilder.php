<?php namespace Hailwood\KnpMenu;

use Knp\Menu\FactoryInterface;

class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    protected $factory;

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
	 * @return \Knp\Menu\ItemInterface
	 */
    public function create() {}
}
