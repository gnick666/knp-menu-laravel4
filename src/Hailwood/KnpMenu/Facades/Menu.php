<?php

namespace Hailwood\KnpMenu\Facades;

class Menu extends \Illuminate\Support\Facades\Facade
{
    /**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
    protected static function getFacadeAccessor() { return 'knp_menu.helper'; }
}
