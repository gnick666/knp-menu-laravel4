<?php namespace Hailwood\KnpMenu\Provider;

use Illuminate\Support\Str;
use Knp\Menu\Provider\MenuProviderInterface;
use Illuminate\Container\Container;

class ContainerAwareProvider implements MenuProviderInterface
{
    /**
     * @var \Illuminate\Container\Container
     */
    private $container;

    /**
     * @var array
     */
    private $menus;

    public function __construct(Container $container, array $menus = array())
    {
        $this->container = $container;
        $this->menus     = $menus;
    }

    public function get($name, array $options = array())
    {
        $name = Str::snake($name);
        if (!isset($this->menus[$name])) {
            $this->menus[$name] = 'knp_menu.'.$name;
            /** @var \Hailwood\KnpMenu\RouterAwareFactory $factory */
            $factory = $this->container['knp_menu.factory'];
            $this->container[$this->menus[$name]] = $factory->createItem('root');
        }

        return $this->container[$this->menus[$name]];
    }

    public function has($name, array $options = array())
    {
        return isset($this->menus[$name]);
    }
}
