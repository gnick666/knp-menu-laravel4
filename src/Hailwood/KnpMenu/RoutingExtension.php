<?php namespace Hailwood\KnpMenu;

use Knp\Menu\Factory\ExtensionInterface;
use Knp\Menu\ItemInterface;
use Illuminate\Routing\UrlGenerator;

class RoutingExtension implements ExtensionInterface
{

    protected $generator;

    public function __construct(UrlGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function buildOptions(array $options)
    {
        if (! empty( $options['route'] )) {
            $parameters     = isset( $options['routeParameters'] ) ? $options['routeParameters'] : array();
            $absolute       = isset( $options['routeAbsolute'] ) ? $options['routeAbsolute'] : false;
            $method         = ( false === strpos($options['route'], '@') ) ? 'route' : 'action';
            $options['uri'] = $this->generator->$method($options['route'], $parameters, $absolute);

            // adding the item route to the extras under the 'routes' key (for the RouteVoter)
            $options = array_merge_recursive(array( 'extras' => array(
                   'routes'            => array( $options['route'] ),
                   'routes_parameters' => array( $options['route'] => $parameters ),
               ) ),
                                             $options);
        }

        return $options;
    }

    public function buildItem(ItemInterface $item, array $options) {}
}
