<?php namespace Hailwood\KnpMenu\Voter;

use Knp\Menu\ItemInterface;
use Illuminate\Routing\Router;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\HttpFoundation\Request;

class RouteVoter implements VoterInterface
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Router
     */
    private $router;

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function matchItem(ItemInterface $item)
    {
        if (null === $this->request) {
            return null;
        }

        $route = $this->router->getCurrentRoute();
        $routeName = $route->getName();

        $action = $route->getAction();
        $route = $routeName ?: $route->getPath();

        if (null === $route) {
            return null;
        }

        $routes     = (array) $item->getExtra('routes', array());
        $parameters = (array) $item->getExtra('routes_parameters', array());

        foreach ($routes as $testedRoute) {

            if ($route !== $testedRoute and $action !== $testedRoute) {
                continue;
            }

            $subject = $testedRoute === $action ? $action : $route;

            if ($subject && isset($parameters[$subject])) {
                foreach ($parameters[$subject] as $name => $value) {
                    if ($this->request->instance()->attributes->get($name) != $value) {
                        return null;
                    }
                }
            }

            return true;
        }

        return null;
    }
}
