Laravel integration for knplabs/knp-menu
========================================

[![Magnum CI](https://magnum-ci.com/status/44501eb44d390aa1bdbd0773d562486b.png)](https://magnum-ci.com/projects/1660)

Provides Laravel 4 integration with [knplabs/knp-menu](http://github.com/KnpLabs/KnpMenu).

> This package is a fork of an existing package
[neatqc/knp-menu](https://github.com/neatqc/knp-menu/tree/master/src/Neat/KnpMenu) that appears to no longer be updated and 60% of the work has come from this developer.


Features
--------

* All KnP-menu features
* Laravel syntax
* Laravel Facade support
* Blade templating language support
* Laravel routing support

Installation
------------

### Install the package

You can simply add this laravel package since it's register on [packagist](https://packagist.org/packages/hailwood/knp-menu-laravel4).

Execute this line in your terminal to install it with composer.

```bash
$ composer require 'hailwood/knp-menu-laravel4:~1.0'
```

### Add the service provider

In `app.php` add `Hailwood\KnpMenu\KnpMenuServiceProvider` to your providers array.

### Add the facade

In `app.php` add `'Menu' => 'Hailwood\KnpMenu\Facades\Menu'` to your aliases array.

### Add the menus directory

We need a new directory in the app path to store the menus.
Create a new directory in your app directory called menus by default this should look like `/app/menus`.

### Publish the config file

If we want to customize the menus we need to publish the config file.
Run the command below which will give you a configuration file at `/app/config/packages/hailwood/knp-menu-laravel4/config.php` that you can edit.

```bash
$ php artisan config:publish hailwood/knp-menu-laravel4
```

Configuration Options
---------------------

###view

The name of the view to use as a template to render the menu.
By default it uses `/vendor/hailwood/knp-menu-laravel4/src/views/default.blade.php`.

Feel free to copy this file to your views directory.
Make the changes you need and update the value in the configuration file.

> The view can be changed at runtime by calling `->setExtra('view', 'view.name')` on the root menu
  object or by passing `view' => 'view.name` in an array as the second parameter of the render method.

###charset

The charset to use when rendering the menus.
We use UTF-8 by default as it provides the largest character set but if your pages are using a different charset then you'll want to change this.

###renderers

An array of rendering engines we can use to parse the above templates.

###default_renderer

The default renderer to use to parse the above template, if it is not changed we will just use this. Since this is a Laravel 4 integration package we have set this to the sensible default of `blade`/

Using predefined menus
----------------------

Menus can be predefined in the `app/menus` directory in files that follow the `MenuNameMenu.php` naming convention.

These menus follow the general stub of:

```php
<?php
use Hailwood\KnpMenu\MenuBuilder;

class MenuNameMenu extends MenuBuilder
{

    /**
     * Returns an instance of the menu created by the builder.
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function create()
    {
        $menu = $this->factory->createItem('root');

        return $menu;
    }
}
```

But the easiest way to generate these menus is using the power of artisan:

```bash
$ php artisan menu:make MenuName
```

The menu can be interacted with using any of the methods from the [official KnpMenu documentation](https://github.com/KnpLabs/KnpMenu) in the `create()` method.

Using the Facade
----------------

Existing menus can be interacted with using the `Menu::` facade we registered earlier such as rendering the menu:

```php
<?php

echo Menu::render('MenuName');
```

If you need to interact with the menu before you render it you can use `Menu::get('MenuName')` to fetch the menu, interact with it as above and then render it. As an example:

```php
<?php

Menu::get('MenuName')->addChild('dog');
Menu::get('MenuName')->addChild('cat');
Menu::get('MenuName')->getChild('dog')->addChild('puppy');

echo Menu::render('MenuName');
```

Generating menus on the fly
---------------------------

We know that you are not always going to know what your menus are before you need them, or maybe you want to generate a menu that's used only in one view.
It seems dirty to create _placeholder_ menus incase you want to use them so instead we give you the ability to generate them on the fly.

```php
<?php

Menu::get('IDontExist')->addChild('birds');
```

Hey that looks familiar! Yep, if `get()` is called with the name of a menu that does not exist we will create it for you right then and there!

HTML menu items
---------------

The blade render has `'allow_safe_labels'` set to `true` which means that calling `->setExtra('safe_label', true)` on any menu item will prevent the renderer from escaping the contents automatically meaning raw html can be used.

The menu and routes
-------------------

This package is tightly integrated with the Laravel routes.
Instead of specifying a `uri` on the menu items specify an action or a route in the `route` key and we will do the rest.
This supports controller actions or named routes.

```php
<?php

$menuItem
     ->addChild('Controller Action', array('route' => 'method@controller'))
     ->addChild('Named Route', array('route' => 'some.named.route'))
;
```

This allows us to also correctly mark which route is the currently active menu item.


More documentation coming soon!
