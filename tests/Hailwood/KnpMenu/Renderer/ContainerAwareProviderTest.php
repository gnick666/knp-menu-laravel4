<?php namespace Tests\Hailwood\KnpMenu\Renderer;

use PHPUnit_Framework_TestCase as TestCase;
use Illuminate\Container\Container;
use Hailwood\KnpMenu\Renderer\ContainerAwareProvider;

class ContainerAwareProviderTest extends TestCase {

	public function testHas()
	{
		$provider = new ContainerAwareProvider(new Container(), 'first', array(
			'first' => 'first',
			'second' => 'dummy',
		));

		$this->assertTrue($provider->has('first'));
		$this->assertTrue($provider->has('second'));
		$this->assertFalse($provider->has('third'));
	}

	public function testGetExistentRenderer()
	{
		$renderer = $this->getMock('Knp\Menu\Renderer\RendererInterface');
		$container = new Container();
		$container['knp_menu.renderer.default'] = $renderer;
		$provider = new ContainerAwareProvider($container, 'default', array(
			'default' => 'knp_menu.renderer.default',
			'custom' => 'other',
		));

		$this->assertSame($renderer, $provider->get('default'));
	}

	public function testGetDefaultRenderer()
	{
		$renderer = $this->getMock('Knp\Menu\Renderer\RendererInterface');
		$container = new Container();
		$container['knp_menu.renderer.default'] = $renderer;
		$provider = new ContainerAwareProvider($container, 'default', array(
			'default' => 'knp_menu.renderer.default',
			'custom' => 'other',
		));

		$this->assertSame($renderer, $provider->get());
	}

	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testGetNonExistentRenderer()
	{
		$provider = new ContainerAwareProvider(new Container(), 'default', array());
		$provider->get('non-existent');
	}
}
