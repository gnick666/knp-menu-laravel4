<?php namespace Tests\Hailwood\KnpMenu\Console\Command;

use Mockery as m;
use PHPUnit_Framework_TestCase as TestCase;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Input\ArrayInput;
use Hailwood\KnpMenu\Console\Command\MakeMenuCommand;

class MakeMenuCommandTest extends TestCase {

	public function tearDown()
	{
		m::close();
	}

	public function testGeneratorIsCalledWithProperOptions()
	{
		$command = new MakeMenuCommand($generator = m::mock('Hailwood\KnpMenu\Generators\MenuGenerator'), __DIR__);
		$generator->shouldReceive('make')->once()->with('FooMenu', __DIR__, array())->andReturn(true);
		$this->runCommand($command, array('name' => 'FooMenu'));
	}

	public function testGeneratorIsCalledWithProperOptionForPath()
	{
		$command = new MakeMenuCommand($generator = m::mock('Hailwood\KnpMenu\Generators\MenuGenerator'), __DIR__);
		$generator->shouldReceive('make')->once()->with('FooMenu', __DIR__.'/foo/bar', array());
		$command->setLaravel(array('path.base' => __DIR__.'/foo'));
		$this->runCommand($command, array('name' => 'FooMenu', '--path' => 'bar'));
	}

	public function runCommand($command, $input = array(), $output = null)
	{
		$output = $output ?: new NullOutput;

		return $command->run(new ArrayInput($input), $output);
	}
}
