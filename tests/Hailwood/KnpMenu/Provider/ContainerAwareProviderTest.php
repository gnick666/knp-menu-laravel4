<?php namespace Tests\Hailwood\KnpMenu\Provider;

use Knp\Menu\MenuFactory;
use PHPUnit_Framework_TestCase as TestCase;
use Illuminate\Container\Container;
use Hailwood\KnpMenu\Provider\ContainerAwareProvider;

class ContainerAwareProviderTest extends TestCase {

	public function testHas()
	{
		$provider = new ContainerAwareProvider(new Container(), array(
			'first' => 'first',
			'second' => 'second',
		));

		$this->assertTrue($provider->has('first'));
		$this->assertTrue($provider->has('second'));
		$this->assertFalse($provider->has('third'));
	}

	public function testGetExistentMenu()
	{
		$provider = new ContainerAwareProvider($container = new Container(), array(
			'default' => 'knp_menu.default',
		));

		$container['knp_menu.default'] = $menu = $this->getMock('Knp\Menu\ItemInterface');

		$this->assertSame($menu, $provider->get('default'));
	}

	public function testGetNonExistentMenu()
	{
        $container = new Container();
        $container['knp_menu.factory'] = new MenuFactory();
		$provider = new ContainerAwareProvider($container);
		$item = $provider->get('non-existent');

        $this->assertInstanceOf('Knp\Menu\ItemInterface', $item);
	}
}
